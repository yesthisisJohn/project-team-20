import java.sql.*;
// import org.postgresql.geometric.PGpoint;
// import java.util.ArrayList;
// import java.util.Collections;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.Set;


/*
psql postgres://bopblzmpjnllky:Am58XAeDwRKHQ8rNhXlQjJriSd@ec2-50-19-227-171.compute-1.amazonaws.com:5432/dcn68bitovrdfv

once you're in postgreSQL

\connect dcn68bitovrdfv

then do the sql commands
*/

public class DatabaseConnection {

    Connection conn;
    String url;
    PreparedStatement pStatement;
    ResultSet rs;
    String queryString;

    DatabaseConnection() throws SQLException {
      try {
          Class.forName("org.postgresql.Driver");
      } catch (ClassNotFoundException e) {
          e.printStackTrace();
      }
   }

   public static void main(String[] args) {

      try {
        Class.forName("org.postgresql.Driver");
      }
      catch (ClassNotFoundException e) {
        System.out.println("Failed to find the postgresql driver");
      }

      try {

        URI dbUri = new URI(System.getenv("DATABASE_URL"));
        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        // url connection during csc343
        // url = "jdbc:postgresql://localhost:5432/csc343h-g5wuvict";
        //url = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
        //conn = DriverManager.getConnection(url, username, password);

        url = "psql postgres://bopblzmpjnllky:Am58XAeDwRKHQ8rNhXlQjJriSd@ec2-50-19-227-171.compute-1.amazonaws.com:5432/dcn68bitovrdfv"
        conn = DriverManager.getConnection(url, username, password);
        //Should be connected now

        //Creating the table of all users
        //Users = (uID, firstName, lastName, email)
        queryString = "CREATE TABLE Users " +
                      "(uID TEXT PRIMARY KEY NOT NULL, " +
                      "email TEXT NOT NULL, " +
                      "firstName TEXT, " +
                      "lastName TEXT);";
        pStatement = conn.prepareStatement(queryString);
        rs = pStatement.executeQuery();
        //Just to see if it worked
        while (rs.next()) {
          String uID = rs.getString("uID");
          String email = rs.getString("email");
          String firstName = rs.getString("firstName");
          String lastName = rs.getString("lastName");
          System.out.println("uID: " + uID);
          System.out.println("email: " + email);
          System.out.println("firstName: " + firstName);
          System.out.println("lastName: " + lastName);
        }
//make events have a description, name of event, ismutable boolean, priority 1-5
        //Creating table of all events
        //Events = (eventID, hostID, uID, startTime, startDate, endTime, endDate)
        queryString = "CREATE TABLE Events " +
                      "(eventID TEXT PRIMARY KEY NOT NULL, " +
                      "hostID TEXT FOREIGN KEY NOT NULL REFERENCES Users(uID), " +
                      "uID TEXT FOREIGN KEY REFERENCES Users(uID), " +
                      "startTime TEXT NOT NULL, " +
                      "endTime TEXT NOT NULL, " +
                      "startDate TEXT NOT NULL, " +
                      "endDate TEXT NOT NULL);";
        pStatement = conn.prepareStatement(queryString);
        rs = pStatement.executeQuery();
        //Just to see if it worked
        while (rs.next()) {
          String eventID = rs.getString("eventID");
          String hostID = rs.getString("hostID");
          String uID = rs.getString("uID");
          String startTime = rs.getString("startTime");
          String endTime = rs.getString("endTime");
          String startDate = rs.getString("startDate");
          String endDate = rs.getString("endDate");
          System.out.println("eventID: " + eventID);
          System.out.println("hostID: " + hostID);
          System.out.println("uID: " + uID);
          System.out.println("startTime: " + startTime);
          System.out.println("endTime: " + endTime);
          System.out.println("startDate: " + startDate);
          System.out.println("endDate: " + endDate);
        }


        //Next queries will be based on Android app
        //I should make a method to create partial queries such as select, insert, update, delete
        
        /*
        example insert query
        queryString = "INSERT INTO Users VALUES " + 
        "('victorwu@gmail.com', 'NotAReal', 'GmailAccount'), " +
        "('cantbenull@gmail.com', 'notNull', 'ahlie'), " +
        "('cantremembermyrealgmailaccount@gmail.com', 'woops', 'mybad');";


		queryString = "INSERT INTO Users VALUES " + "('" + uIDvariable + "', '" + emailVariable;
		if (first name is given) {
			queryString = queryString + "', '" + firstNameVariable";
		}
		if (last name is given) {
			queryString = queryString + "', '" + lastNameVariable";
		}
		queryString = queryString + "');";


        example select query
        queryString = "SELECT * " +
                      "FROM EVENTS " +
                      "WHERE HOSTEMAIL = givenemail;";
        */

        conn.close();
      }
   }

}
