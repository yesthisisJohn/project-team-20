# Conrtibuting to TaskMaster
To ensure that our repository is easily accessible for every member of our team as well as for whoever marks us for each deliverable, it is proposed that each contribution to the main branch of our repository follow these standards.

### General
Under no circumstances should you commit anything in the root directory of this repo unless it's a `.md` or a new folder. All resources should be neatly organized in their respective folders. If you wish to implement a new features and wish to have a platform to test it, **fork this repository** instead of adding a new file somewhere in the project folder. All files commited to master should be considered presentable work.

### Android Studio 
**DO NOT** randomly place files inside the Android studio folder. If you do not know where to add a file, please ask someone first instead of arbitrarily placing it in the root. We do not have the time to go back and manually combine your code with the rest of the project. This way, we can **ensure that no two people work on the same set of features**. 

### Research 
Descriptions should be typed up in `.md` files rather than `.txt`. Markdown provides better readability and the syntax is very easy to understand. **You should use [this](http://dillinger.io/)** or the native Github Markdown editor to write any text documents you wish to include. You cannnot under any circumstances contribute files such as `.doc` or `.pdf`. The language of your descriptions must be **professional and concise**. 
If you wish to contribute a piece of research you have completed for this project, you **must** write it to the `/research` folder. This may include anything along the lines of: 
* reference material, API documentation
* proposed design guidelines
* description of ideas, algorithms, etc.

You may choose to create a new directory inside this `/research` should you have many files to add. If you choose to add a new folder, please include a `README.md` file detailing how the content of your folder will benefit the overall project.

This folder may **not contain any code.**

### Backend
If you want to add a piece of code related to the algorithm or core functionalities of TaskMaster, your code **must** be commmited in `app/src/main/java/backend`. This will allow those who are working on the Android portions of the project to have access to your implementation without having to copy/move files around. **Do not include any files that are not ready to be called upon by anyone implementing the Android app.** If your file is not ready, use a personal branch or add it to a folder in `/research`. 

### Server
Any files pertaining to the SQL Server should be contained in `/server`. It should be abstracted away from anyone not working on the server. There shall be `README.md` in this folder describing how to make basic queries to this database. 

### UI
Any files pertaining front-end or UI design must be contained in the Android project as dictated by Android Studio. **Do NOT include large pictures** in this repository, use a link to an external source such as GoogleDrive or Dropbox instead. This is so that we do not have to download the pictures everytime we do a new clone. 
   
