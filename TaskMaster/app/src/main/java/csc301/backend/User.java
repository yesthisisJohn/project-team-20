package csc301.backend;

import android.os.MessageQueue;
import java.util.UUID;

/**
 * Created by jiyko on 11/30/2016.
 */
public class User {

    private String uID;
    private String email;
    private String firstName;
    private String lastName;



    // WARNING: it's up to whoever instantiates these classes to implement the UUID key for the IDs
    // String id = UUID.randomUUID().toString();
    public User(String uID, String email, String first, String last) {

        this.uID = uID;
        this.email = email;
        this.firstName = first;
        this.lastName = last;
    }

    public String getEmail() { return email; }

    public String getuID() { return uID; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    @Override
    public String toString() {
        return uID + "," + email + "," + firstName + "," + lastName;
    }

    public boolean equals(User other) {
        return this.getuID().equals(other.getuID());
    }

}
