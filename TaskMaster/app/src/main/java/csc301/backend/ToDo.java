package csc301.backend;


import java.util.Calendar;

/**
 * Created by Mingchen on 2016-12-02.
 */

public class ToDo implements Comparable<ToDo>{
    private int durationInHours;
    private Calendar start, deadline;

    public ToDo(int durationInHours, Calendar start, Calendar deadline) {
        this.durationInHours = durationInHours;
        this.deadline = deadline;
        this.start = start;

        // Add a checker to ensure that the time between start and deadline have enough
        // hours
    }

    public Calendar getDeadline() {
        return this.deadline;
    }

    public int getDurationInHours() {
        return this.durationInHours;
    }

    public void setDurationInHours(int durationInHours) {
        this.durationInHours = durationInHours;
    }

    @Override
    public int compareTo(ToDo toDo) {
        return this.deadline.compareTo(toDo.getDeadline());
    }

    public Calendar getStart() {
        return this.start;
    }
}