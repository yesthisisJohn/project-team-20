package csc301.backend;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mingchen on 2016-12-02.
 */

public interface SchedulingAlgorithm {
    public List<mTask> scheduleToDos(List<Task> todos);

}
