package csc301.backend;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * The 'big' Task from which the algorithm builds mini 'mTasks.'
 * Utilized by DatabaseConnection, SchedulingAlgorithm.
 */
public class Task implements Comparable<Task> {

	private String eID;
	private String hostID;
	private String pID;

	private String title;
	private String description;
	private boolean isMutable;

	private String datePattern;
	private SimpleDateFormat format;
	private Date startDate;
	private Date endDate;
	private String sDateString;
	private String eDateString;

	// these are set when dealing with mutable tasks
	private int durationInHours;
	private int priority;


	// WARNING: it's up to whoever instantiates these classes to implement the UUID key for the IDs
	// This should have been a builder at this point.
	// String id = UUID.randomUUID().toString();
	public Task(String eID, String hostID, String pID, String sDate, String eDate,
				String title, String desc, boolean isMut, int priority, int expectedHours) {

		if (sDate == null || eDate == null) {
			throw new NullPointerException();
		}

		if (sDate.equals("") || eDate.equals("")) {
			throw new IllegalArgumentException();
		}

		if (title == null) {
			throw new IllegalArgumentException();
		} else {
			setTitle(title);
		}
		// set a date pattern to handle database reads/writes of dates
		datePattern = "yyyy-MM-dd HH:mm:ss";
		format = new SimpleDateFormat(datePattern);

		try {
			setStartDate(format.parse(sDate));
			setEndDate(format.parse(eDate));
		} catch (ParseException e) {
			//e.printStackTrace();
			setStartDate(null);
			setEndDate(null);
		}

		setDescription(desc);
		setMutable(isMut);

		this.eID = eID;
		this.hostID = hostID;
		this.pID = pID;
		this.sDateString = sDate;
		this.eDateString = eDate;

		setPriority(priority);
		setDuration(expectedHours);

	}

	///////// S E T T E R S /////////

	public void setDescription(String description) {
		if (description == null) {
			this.description ="";
		} else {
			this.description = description;
		}
	}

	public void setMutable(boolean mutable) { this.isMutable = mutable; }

	public void setStartDate(Date startDate) { this.startDate = startDate; }

	public void setEndDate(Date endDate) { this.endDate = endDate; }

	public void setTitle(String title) { this.title = title; }

	/**
	 * Set the duration in hours iff the task is mutable.
	 * @param durationInHours - the hours this task takes to complete
     */
	public void setDuration(int durationInHours) {
		if (isMutable) {
			this.durationInHours = durationInHours;
		} else {
			// based on start and end dates
//			System.out.println("!setting duration of task based on dates!");
			this.durationInHours = (int) (getDurationFromDates() / 60);
		}
	}

	/**
	 * Set the priority of this task
	 * 3 most important | 2 important | 1 least important
	 * Note:
	 * task importance is simply for coloring tasks at the user level
	 * so the user knows the importance of each task
	 *
	 * @param newPriority the priority the user gives this task
     */
	public void setPriority(int newPriority) {
		if (!isMutable) {
			// this is an immutable task--assumed to be important
			this.priority = 3;

		} else if (newPriority > 3) {
			this.priority = 3;
		} else if (newPriority < 1) {
			this.priority = 1;
		} else {
			this.priority = newPriority;
		}
	}

	///////// G E T T E R S /////////

	public String geteID() { return this.eID; }

	public String getHostID() { return this.hostID; }

	public String getpID() { return this.pID; }

	public String getTitle() { return this.title; }

	public String getDescription() { return this.description; }

	public Date getStartDate() {
//		System.out.println("getStartDate: " + startDate);
		return startDate; }

	/**
	 * Returns the startDate converted from date type to calendar type.
	 * @return startDate in Calendar format
	 */
	public Calendar getStart() {
		return dateToCalendar(getStartDate());
	}

	public String getStartString() {
//		System.out.println("getStartString: " + sDateString);
		return sDateString;
	}

	public Date getEndDate() {
//		System.out.println("getEndDate: " + endDate);
		return endDate; }

	/**
	 * Returns the endDate converted from date type to calendar type.
	 * @return endDate in Calendar format
     */
	public Calendar getDeadline() {
		return dateToCalendar(getEndDate());
	}

	public String getEndString() {
//		System.out.println("getEndString: " + eDateString);
		return eDateString;
	}

	public boolean getIsMutable() { return isMutable; }

	public SimpleDateFormat getFormat() { return format; }

	/**
	 * Returns the duration inputed by the user for how long
	 * this task will take
	 * @return durationInHours
     */
	public int getDurationInHours() { return this.durationInHours; }

	/**
	 * Returns the duration inputed by the user based on
	 * the starting and ending dates chosen for this task
	 * @return difference between eDateString and sDateString
     */
	public long getDurationFromDates() {
		return findDuration(this.eDateString, this.sDateString);
	}

	public int getPriority() { return this.priority; }

	///////// H E L P E R S /////////

	/**
	 * Return the duration between eDate and sDate in minutes
	 * @param eDate the string form of the end date for this task
	 * @param sDate the string form of the start date for this task
	 */
	public long findDuration(String eDate, String sDate) {
		long duration = 0;
		if (eDate == null || sDate == null) {
			throw new NullPointerException();
		}
		if (eDate.equals("") || sDate.equals("")) {
			throw new IllegalArgumentException();
		}
		try {
			duration = computeDuration(getFormat().parse(eDate),
					getFormat().parse(sDate),
					TimeUnit.MINUTES);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return duration;
	}

	/**
	 * Get the duration between two dates
	 * @param dateEnd the later date
	 * @param dateStart the older date
	 * @param tUnit the unit in which you want the duration
	 * @return the duration value in the provided unit tUnit
	 */
	private static long computeDuration(Date dateEnd, Date dateStart, TimeUnit tUnit) {
		long durInMilli = dateEnd.getTime() - dateStart.getTime();
		return tUnit.convert(durInMilli, TimeUnit.MILLISECONDS);
	}

	/**
	 * Return the Calendar type of this Date date
	 * @param date
	 * @return calendar cal
     */
	public static Calendar dateToCalendar(Date date){
		if (date == null) {
			throw new NullPointerException("dateToCalendar threw date");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
//		cal.set(date.getYear(),
//				date.getMonth(),
//				date.getDay(),
//				date.getHours(),
//				date.getMinutes());
		return cal;
	}

	@Override
	public int compareTo(Task other) {
		return this.getDeadline().compareTo(other.getDeadline());
	}

	/**
	 * String of this
	 * @return this string
     */
	@Override
	public String toString() {
		return title + ",start: " + startDate + ",end: " + endDate;
	}


	public boolean equals(Task other) {
		return this.geteID().equals(other.geteID());
	}
}
