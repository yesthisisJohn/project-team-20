package csc301.backend;

import android.provider.BaseColumns;
/**
 * Created by albertxie on 29/11/16.
 */

public final class DatabaseContract {

    private DatabaseContract() {}

    public static class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "Users";
        public static final String UID = "uID";
        public static final String EMAIL = "email";
        public static final String FIRSTNAME = "firstName";
        public static final String LASTNAME = "lastName";
    }

    public static class TaskEntry implements BaseColumns {
        public static final String TABLE_NAME = "Tasks";
        public static final String TID = "tID";
        public static final String HOST = "hostID";
        public static final String PARTICIPANT = "participantID";
        public static final String START = "startTime";
        public static final String END = "endTime";
        public static final String TITLE = "taskName";
        public static final String DESCRIPTION = "taskDescription";
        public static final String MUTABLE = "isMutable";
        public static final String PRIORITY = "priority";
        public static final String EXPECTEDHOURS = "expectedHours";
    }
}
