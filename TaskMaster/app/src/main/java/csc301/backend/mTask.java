package csc301.backend;
import android.graphics.Color;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * mTask is the actual task for the calendar to be displayed
 * Created by Mingchen on 2016-12-02.
 * Edited by John
 */

public class mTask implements Serializable{
    private Calendar start, end;
    private String title, description;

    private int priority;
    private boolean isMutable;
    Locale locale;

    private String PrimaryColor;
    private String DarkColor;
    private String SecondDarkColor;

    public mTask(String title, String desc, Calendar start, Calendar end, boolean isMut, int priority) {

        setTitle(title);
        setDescription(desc);

        setStartDate(start);
        setEndDate(end);
        setMutable(isMut);
        setPriority(priority);

        // Some colors to use for an mTask
        PrimaryColor    = "#3F51B5"; // PrimaryBlue
        DarkColor       = "#303F9F"; // DarkBlue
        SecondDarkColor = "#8B008B"; // DarkMagenta


        locale = Locale.getDefault();
    }

    public boolean checkConflict(Calendar lower, Calendar upper) {

        return this.start.getTime().before(upper.getTime()) && this.end.getTime().after(lower.getTime());
    }

    ///////// S E T T E R S /////////

    public void setTitle(String title) { this.title = title; }

    public void setMutable(boolean mutable) { this.isMutable = mutable; }

    public void setStartDate(Calendar startDate) { this.start = startDate; }

    public void setEndDate(Calendar endDate) { this.end = endDate; }

    public void setDescription(String description) {
        if (description == null) {
            this.description ="";
        } else {
            this.description = description;
        }
    }

    public void setPriority(int newPriority) {
        if (!isMutable) {
            // this is an immutable task--assumed to be important
            this.priority = 3;

        } else if (newPriority > 3) {
            this.priority = 3;
        } else if (newPriority < 1) {
            this.priority = 1;
        } else {
            this.priority = newPriority;
        }
    }

    ///////// G E T T E R S /////////

    public String getTitle() { return this.title; }

    public String getDescription() { return this.description; }

    public int getPriority() { return this.priority; }

    public boolean getIsMutable() { return isMutable; }

    public String getStartStr() {
        return this.start.getTime().toString();
    }

    public String getEndStr() {
        return this.end.getTime().toString();
    }

    public int getDay() {
        return this.start.get(Calendar.DAY_OF_MONTH);
        //return this.start.getTime().getDay();
    }

    public Locale getLocale() {
        return locale;
    }

    public boolean isMutable() {
        return isMutable;
    }

    public Calendar getStart() {
        return start;
    }

    public Calendar getEnd() {
        return end;
    }

    /**
     * Return a more readable version of mTask for the user to see
     * in the form:
     * 'title' ' from: HH:mm' ' to: HH:mm'
     * @return a string of title, start time and end time
     */
    public String displayString() {
        String shour = makeTimeString(this.getStart().getTime().getHours());
        String smin = makeTimeString(this.getStart().getTime().getMinutes());
        String ehour = makeTimeString(this.getEnd().getTime().getHours());
        String emin = makeTimeString(this.getEnd().getTime().getMinutes());

        return this.getTitle() + " from: " + shour + ":" + smin +
                " to: " + ehour + ":" + emin;
    }

    /**
     * Create a string version of this stringTime
     * Ensure it is size 2.
     * > makeTimeString(9)
     * > "09"
     * > makeTimeString(12)
     * > "12"
     * @param thisTime the time in minutes or hours as a string
     * @return the same time but as a string
     */
    private String makeTimeString(int thisTime) {
        String s = "0" + thisTime;
        if (s.length() > 2) {
            s = s.substring(1);
        }
        return s;
    }

    /**
     * Return the color for this mTask based on its priority.
     * @return an int for this mTask's color
     */
    public int getColorFromPriority() {
        int color = Color.BLACK;
        if (getPriority() == 3) {
            color = Color.parseColor(PrimaryColor);
        } else if (getPriority() == 2) {
            color = Color.parseColor(DarkColor);
        } else if (getPriority() == 1) {
            color = Color.parseColor(SecondDarkColor);
        }
        return color;
    }

    @Override
    public String toString() {
        return this.getTitle() + " from: " + getStartStr() + " to " + getEndStr();
    }
}
