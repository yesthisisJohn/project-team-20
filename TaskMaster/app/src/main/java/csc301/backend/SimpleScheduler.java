package csc301.backend;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Mingchen on 2016-12-02.
 */

public class SimpleScheduler implements SchedulingAlgorithm {
    // set these to desired work day routine--tasks can only be scheduled within these times
    private int workDayStart = 9;
    private int workDayEnd = 17; // 24-hour time

    private ArrayList<Integer> workDays = new ArrayList<Integer>();

    public SimpleScheduler() {
        this.workDays.add(Calendar.MONDAY);
        this.workDays.add(Calendar.TUESDAY);
        this.workDays.add(Calendar.WEDNESDAY);
        this.workDays.add(Calendar.THURSDAY);
        this.workDays.add(Calendar.FRIDAY);
    }

    public ArrayList<mTask> scheduleToDos(List<Task> todos) {
        Collections.sort(todos);

        ArrayList<mTask> sortedTasks = new ArrayList<mTask>();
        Calendar start = new GregorianCalendar();

        for (Task todo : todos) {    // Add equals method for class ToDo
            scheduleTodo(todo, sortedTasks);
        }
        return sortedTasks;
    }

    private void scheduleTodo(Task toDo, List<mTask> sortedTasks) {
        if (toDo == null) {
            throw new NullPointerException("Task is null in scheduleTodo");
        }
        if (sortedTasks == null) {
            throw new NullPointerException("sortedTask is null in scheduleTodo");
        }
        Calendar start = (Calendar) toDo.getStart().clone();
        Calendar end = (Calendar) toDo.getDeadline().clone();
        int hoursWorking = 0;

        // get necessary metadata
        String title = toDo.getTitle();
        String desc = toDo.getDescription();
        boolean isMut = toDo.getIsMutable();
        int priority = toDo.getPriority();

        int startHour = start.get(Calendar.HOUR_OF_DAY);
        int endHour = end.get(Calendar.HOUR_OF_DAY);

        while(!start.after(toDo.getDeadline()) && hoursWorking < toDo.getDurationInHours()){

            if (workDays.contains(start.get(Calendar.DAY_OF_WEEK))) {
                for (int i = workDayStart; i < workDayEnd; i++) {
                    // do not schedule the task before the start time or after the deadline
                    if (i < startHour || i > endHour) {
                        continue;
                    }

                    // ensure we are adding this task to a time within the task's boundaries
                    if ((workDayStart <= startHour && startHour <= workDayEnd) ||
                            workDayStart <= endHour && endHour <= workDayEnd) {
                        Calendar timeslot = Calendar.getInstance();
                        timeslot.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH),
                                start.get(Calendar.DATE), i, 0, 0);
                        Calendar currEnd = (Calendar) timeslot.clone();
                        currEnd.add(Calendar.HOUR_OF_DAY, 1);

                        if (checkAvailable(timeslot, currEnd, sortedTasks) && i != 12) {
                            sortedTasks.add(new mTask(title, desc, timeslot, currEnd, isMut, priority));
                            hoursWorking++;
                        }
                        // check if everything is scheduled
                        if (hoursWorking == toDo.getDurationInHours()) {
                            return;
                        }

                    }
                }
            }
            start.add(Calendar.DATE, 1);
        }
    }

    private boolean checkAvailable(Calendar timeslot, Calendar end, List<mTask> sortedTasks) {
        for (mTask currentTask: sortedTasks) {
            if (currentTask.checkConflict(timeslot, end)) {
                return false;
            }
        }
        return true;
    }

}
