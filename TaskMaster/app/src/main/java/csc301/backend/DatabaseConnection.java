package csc301.backend;

import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by albertxie on 29/11/16.
 */

public class DatabaseConnection extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "backendData.db";

    private static final String SQL_CREATE_USERS =
            "CREATE TABLE " + DatabaseContract.UserEntry.TABLE_NAME +
            " (uID STRING PRIMARY KEY, " +
            "email STRING NOT NULL, " +
            "firstName STRING NOT NULL, " +
            "lastName STRING NOT NULL);";

    private static final String SQL_CREATE_TASKS =
            "CREATE TABLE " + DatabaseContract.TaskEntry.TABLE_NAME +
            " (tID STRING PRIMARY KEY, " +
            "hostID STRING NOT NULL, " +
            "participantID STRING NOT NULL, " +
            "startTime STRING NOT NULL, " +
            "endTime STRING NOT NULL, " +
            "taskName STRING NOT NULL, " +
            "taskDescription STRING, " +
            "isMutable INTEGER, " +
            "priority INTEGER, " +
            "expectedHours INTEGER);";

    private static final String SQL_DELETE_USERS =
            "DROP TABLE IF EXISTS " + DatabaseContract.UserEntry.TABLE_NAME;

    private static final String SQL_DELETE_TASKS =
            "DROP TABLE IF EXISTS " + DatabaseContract.TaskEntry.TABLE_NAME;

    public DatabaseConnection(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.v("dbConnection", "database onCreate invoked");
        db.execSQL(SQL_CREATE_USERS);
        db.execSQL(SQL_CREATE_TASKS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TASKS);
        db.execSQL(SQL_DELETE_USERS);
        this.onCreate(db);
    }

    public void wipe() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_DELETE_TASKS);
        db.execSQL(SQL_DELETE_USERS);
        this.onCreate(db);
        db.close();
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, newVersion, oldVersion);
    }

    public void addUser(User user) {
        // Gets the data repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.UserEntry.UID, user.getuID());
        values.put(DatabaseContract.UserEntry.EMAIL, user.getEmail());
        values.put(DatabaseContract.UserEntry.FIRSTNAME, user.getFirstName());
        values.put(DatabaseContract.UserEntry.LASTNAME, user.getLastName());

        // Insert the new row, returning the primary key value of the new row
        db.insert(DatabaseContract.UserEntry.TABLE_NAME, null, values);
        db.close();
    }

    public void updateUser(User user) {
        //can probably change to void
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.UserEntry.FIRSTNAME, user.getFirstName());
        values.put(DatabaseContract.UserEntry.LASTNAME, user.getLastName());
        // updating row
        db.update(DatabaseContract.UserEntry.TABLE_NAME, values,
                DatabaseContract.UserEntry.UID + " = ?",
                new String[]{String.valueOf(user.getuID())});
        db.close();
    }

    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseContract.UserEntry.TABLE_NAME, DatabaseContract.UserEntry.UID + " = ?",
                new String[] { String.valueOf(user.getuID()) });
        db.close();// maybe shouldnt close it, not sure
    }

    public void addTask(Task task) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.TaskEntry.TID, task.geteID());
        values.put(DatabaseContract.TaskEntry.HOST, task.getHostID());
        values.put(DatabaseContract.TaskEntry.PARTICIPANT, task.getpID());
        values.put(DatabaseContract.TaskEntry.START, task.getStartString());
        values.put(DatabaseContract.TaskEntry.END, task.getEndString());
        values.put(DatabaseContract.TaskEntry.TITLE, task.getTitle());
        values.put(DatabaseContract.TaskEntry.DESCRIPTION, task.getDescription());
        values.put(DatabaseContract.TaskEntry.MUTABLE, task.getIsMutable());
        values.put(DatabaseContract.TaskEntry.PRIORITY, task.getPriority());
        values.put(DatabaseContract.TaskEntry.EXPECTEDHOURS, task.getDurationInHours());
        db.insert(DatabaseContract.TaskEntry.TABLE_NAME, null, values);
        db.close();
    }

    public void updateTask(Task task) {
        //can probably change to void
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.TaskEntry.TITLE, task.getTitle());
        values.put(DatabaseContract.TaskEntry.DESCRIPTION, task.getDescription());
        // updating row
        db.update(DatabaseContract.TaskEntry.TABLE_NAME, values,
                DatabaseContract.TaskEntry.TID + " = ?",
                new String[]{String.valueOf(task.geteID())});
        db.close();
    }

    public void deleteTask(String eID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseContract.TaskEntry.TABLE_NAME, DatabaseContract.TaskEntry.TID + " = ?",
                new String[] { eID });
        db.close();// maybe shouldnt close it, not sure
    }


    public ArrayList<User> getUsers() {
        ArrayList<User> queryResult = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();

        try {
            Cursor c = db.query(
                    DatabaseContract.UserEntry.TABLE_NAME,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            );

            while (c.moveToNext()) {
                String uid = c.getString(0);
                String email = c.getString(1);
                String first = c.getString(2);
                String last = c.getString(3);
                User user = new User(uid, email, first, last);
                queryResult.add(user);
            }
            c.close();
        } catch (SQLiteAbortException e) {
            Log.v("DatabaseConnection", "SQL Error in getUser");
        }
        db.close();
        return queryResult;
    }


    public ArrayList<Task> getTasks() {
        ArrayList<Task> queryResult = new ArrayList<Task>();
        SQLiteDatabase db = this.getReadableDatabase();


        try {
            Cursor c = db.query(
                    DatabaseContract.TaskEntry.TABLE_NAME,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            );

            while(c.moveToNext()) {

                String tid = c.getString(0);
                String hostID = c.getString(1);
                String pid = c.getString(2);
                String start = c.getString(3);
                String end = c.getString(4);
                String title = c.getString(5);
                String description = c.getString(6);
                int isMutableFlag = Integer.parseInt(c.getString(7));
                boolean isMutable = false;

                if (isMutableFlag == 1) {
                    isMutable = true;
                }

                int priority = Integer.parseInt(c.getString(8));
                int expectedHours = Integer.parseInt(c.getString(9));

                Task task = new Task(tid, hostID, pid, start, end, title, description, isMutable, priority, expectedHours);
                queryResult.add(task);
            }
            c.close();
        } catch (SQLiteAbortException e) {
            Log.v("DatabaseConnection", "SQL Error in getTasks");
        }
        db.close();
        return queryResult;
    }
}