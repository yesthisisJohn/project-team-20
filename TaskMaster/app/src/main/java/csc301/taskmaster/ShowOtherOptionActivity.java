package csc301.taskmaster;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ShowOtherOptionActivity extends AppCompatActivity {
    //creates an alternative option with the event scheduled at 4-7 instead
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_other_option);
    }
    public void acceptEvent(View activity_show_other_option){
        Intent intent = new Intent(this, TaskAcceptedActivity.class);
        startActivity(intent);
    }
}
