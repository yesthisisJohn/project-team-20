package csc301.taskmaster;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import csc301.backend.DatabaseConnection;
import csc301.backend.SchedulingAlgorithm;
import csc301.backend.SimpleScheduler;
import csc301.backend.Task;
import csc301.backend.mTask;


public class CalendarActivity extends AppCompatActivity {

    CalendarView calendarView;
    TextView dateDisplay;
    private ScrollView scrollTasks;
    private int numOfTasks; // the number of tasks for that day

    Map<Integer, ArrayList<mTask>> dayToTask = new HashMap<Integer, ArrayList<mTask>>();
    private String uID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar);

        calendarView = (CalendarView) findViewById(R.id.calendarView);
        dateDisplay = (TextView) findViewById(R.id.task_count_display);

        numOfTasks = 0;
        // list would look like "Title -> startdate, enddate"
        scrollTasks = (ScrollView) findViewById(R.id.scroll_tasks);

        // retrieve user's pre-existing Tasks from the database, if any
        uID = (String) getIntent().getExtras().get("uID");
        DatabaseConnection mDbHelper = new DatabaseConnection(this);

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        //ArrayList<Task> allTasks = mDbHelper.getTasks();

        List<Task> allTasks = mDbHelper.getTasks();

        List<Task> userTasks = new ArrayList<Task>();
        for (Task task : allTasks) {
//            System.out.println("@@@ task: " + task.toString());
            if (task.getHostID().equals(uID) || task.getpID().equals(uID)) {
                userTasks.add(task);
            }
        }

        // DatabaseConnection mDbHelper = new DatabaseConnection(this);
        SchedulingAlgorithm scheduler = new SimpleScheduler();
        List<mTask> scheduledTasks = new ArrayList<mTask>();
        scheduledTasks = scheduler.scheduleToDos(userTasks); // why not userTasks??

        for (mTask task: scheduledTasks) {
//            System.out.println("task day: " + task.getDay());
            // added ! since if dayToTask doesn't have it, you should put it. Right?
            if (!dayToTask.containsKey(task.getDay())) {
//                System.out.println("dayToTask doesn't have this task: " + task.toString());
                dayToTask.put(task.getDay(), new ArrayList<mTask>());
                List<mTask> dayTasks = dayToTask.get(task.getDay());
                dayTasks.add(task);
            } else {
//                System.out.println("dayToTask does have this task: " + task.toString());
                List<mTask> dayTasks = dayToTask.get(task.getDay());
                dayTasks.add(task);
            }
            numOfTasks++;
        }

        // So here we should begin to actually display things on the calendar. Use scrollTasks
        displayTasksInScroll(scheduledTasks, scrollTasks);
        dateDisplay.setText("Tasks Today: " + numOfTasks);

        ///////////// moved down ///////////////
        //TODO: show which days have events in CalendarView

        // everytime we click on a new date, wait for the button to be pressed before moving to dayscroll
        Button setTask = (Button) findViewById(R.id.set_a_task);
        if (setTask != null) {
            setTask.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    setupATask(view, Calendar.getInstance().getTime().getDay());
                }
            });
        }

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int year, int month, int date) {
                //dateDisplay.setText("Date: " + date + " / " + month + " / " + year);
                Intent intent = new Intent(getApplicationContext(), DayScrollActivity.class);
                intent.putExtra("uID", uID);
                intent.putExtra("date", Integer.valueOf(date));
                intent.putExtra("tasks", dayToTask.get(date));
                startActivity(intent);


//                calendarView.setOnClickListener(new View.OnClickListener() {
////                    final List<mTask> todaysTasks = scheduledTasks.c;
//                    @Override
//                    public void onClick(View view) {
////                        displayTasksInScroll(scheduledTasks, scrollTasks);
//                    }
//                });
//                calendarView.setOnLongClickListener(new View.OnLongClickListener() {
//                    @Override
//                    public boolean onLongClick(View view) {
//                        Intent intent = new Intent(getApplicationContext(), DayScrollActivity.class);
//                        intent.putExtra("uID", uID);
//                        System.out.println("date: " + date);
//                        intent.putExtra("tasks", dayToTask.get(date));
//                        startActivity(intent);
//                        return true;
//                    }
//                });
                //Toast.makeText(getApplicationContext(), "Selected Date:\n" + "Day = " + date + "\n" + "Month = " + month + "\n" + "Year = " + i, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Display tasks for the chosen day in the calendar
     * @param tasksToShow all the mTasks this user has
     * @param sView the scrollView to place the LinearLayout in
     */
    private void displayTasksInScroll(List<mTask> tasksToShow, ScrollView sView) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.tasks_layout);
        layout.setBackgroundColor(Color.TRANSPARENT);

        TextView[] tv = new TextView[tasksToShow.size()];

        for (mTask task : tasksToShow) {
            int i = tasksToShow.indexOf(task);
            tv[i] = new TextView(this); //or CalendarActivity.this?
            // setup layout parameters
            tv[i].setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            tv[i].setText(task.displayString());
            tv[i].setTextColor(task.getColorFromPriority());
            layout.addView(tv[i]);
        }

    }

    /**
     * Move from this calendar view directly to TaskActivity for this date
     * @param view the current calendar view
     * @param date the date selected
     */
    public void setupATask(View view, int date) {

        Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
        intent.putExtra("uID", uID);
        intent.putExtra("tasks", dayToTask.get(date));
        startActivity(intent);
    }
}
