package csc301.taskmaster;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;



/**
 * Simple splash screen when app starts.
 * Created by jiyko on 11/29/2016.
 */

public class SplashActivity extends Activity {
    private int duration = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(duration);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();

    }

    @Override
    // Destroys the splash screen so pressing back does not bring the User to
    // the splash screen again.
    protected void onPause() {
        super.onPause();
        finish();
    }

}
