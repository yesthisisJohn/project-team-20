package csc301.taskmaster;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

import csc301.backend.DatabaseConnection;
import csc301.backend.User;

public class SignUpActivity extends AppCompatActivity {

    private EditText firstNameView;
    private EditText lastNameView;
    private EditText emailView;
    private EditText passwordView;
    private EditText confirmPasswordView;
    private Button addUserButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        final DatabaseConnection dbConnection = new DatabaseConnection(getApplicationContext());

        firstNameView = (EditText) findViewById(R.id.addFirstNameField);
        lastNameView = (EditText) findViewById(R.id.addLastNameField);
        emailView = (EditText) findViewById(R.id.addEmailField);
        passwordView = (EditText) findViewById(R.id.addPasswordField);
        confirmPasswordView = (EditText) findViewById(R.id.confirmPasswordField);
        addUserButton = (Button) findViewById(R.id.addUserButton);

        addUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstname, lastname, email, password, confirmPassword;
                firstname = firstNameView.getText().toString();
                lastname = lastNameView.getText().toString();
                email = emailView.getText().toString();
                password = passwordView.getText().toString();
                confirmPassword = confirmPasswordView.getText().toString();

                User user = new User(UUID.randomUUID().toString(), firstname, lastname, email);
                dbConnection.addUser(user);

                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;

                if(!password.equals(confirmPassword)) {
                    CharSequence text = "Passwords do not match.";
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
                else {
                    dbConnection.close();
                    CharSequence text = "Hello " + firstname + ", welcome to TaskMaster!";
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    Intent login = new Intent(getApplicationContext(), CalendarActivity.class);
                    login.putExtra("uID", user.getuID());
                    startActivity(login);
                }
            }
        });

    }
}
