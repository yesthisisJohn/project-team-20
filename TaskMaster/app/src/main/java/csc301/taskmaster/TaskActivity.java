package csc301.taskmaster;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import csc301.backend.DatabaseConnection;
import csc301.backend.SimpleScheduler;
import csc301.backend.Task;
import csc301.backend.mTask;

public class TaskActivity extends AppCompatActivity {

    private String uID;
    private ArrayList<mTask> tasks;

    // CharSequences for toasts based on invalid user input
    private CharSequence validDurationCSeq = "Please input a valid start and/or end date";
    private CharSequence validTitleCSeq = "Task title missing";
    private CharSequence validAmountOfTimeCSeq = "Expected Time Impossible given Deadline!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.uID = (String) getIntent().getExtras().get("uID");
        this.tasks = (ArrayList<mTask>) getIntent().getExtras().get("tasks");
//        System.out.println("tasks obtained: " + tasks);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        Button goBack = (Button) findViewById(R.id.back);
        if (goBack != null) {
            goBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBack(view);
                }
            });
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    public void goToTaskCreated(View activity_task) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //convert inputted date/time to sdf that Task needs ("yyyy-MM-dd HH:mm:ss")
        DatePicker startDatePicker = (DatePicker) findViewById(R.id.date_picker_start);
        TimePicker startTimePicker = (TimePicker) findViewById(R.id.time_picker_start);

        String startDate = makeDate(startDatePicker, startTimePicker, sdf);

        DatePicker endDatePicker = (DatePicker) findViewById(R.id.date_picker_end);
        TimePicker endTimePicker = (TimePicker) findViewById(R.id.time_picker_end);

        String endDate = makeDate(endDatePicker, endTimePicker, sdf);

        EditText title = (EditText) findViewById(R.id.title);
        String titleText = title.getText().toString();

        EditText desc = (EditText) findViewById(R.id.description);
        String descText = desc.getText().toString();

        EditText expectedHours = (EditText) findViewById(R.id.expected);
        int expHours;
        if (expectedHours.getText().toString().length() != 0) {
            expHours = Integer.parseInt(expectedHours.getText().toString());
        } else {
            expHours = endTimePicker.getHour() - startTimePicker.getHour();
        }

        EditText inputPriority = (EditText) findViewById(R.id.priority);
        int priority;
        if (inputPriority.getText().toString().length() != 0) {
            priority = Integer.parseInt(inputPriority.getText().toString());
        } else {
            priority = 3;
        }

        // gives this task a unique eID
        String eID = UUID.randomUUID().toString();
        // ensure the task has a start and end date
        startDate = checkDate(startDate, sdf);
        endDate = checkDate(endDate, sdf);
//        System.out.println(">>>>>>>>>>>>>> " + endDate);

        Task task = new Task(eID, uID, uID, startDate, endDate,
                titleText, descText, true, priority, expHours);

//        System.out.println("@@@@@@@@@@@@@@@@ " + task.getTitle().length());
        if (task.getDurationFromDates() < 0) {
            // notify user to change duration
            toastTheUser(validDurationCSeq);
        } else if (task.getDurationInHours() > (task.getDurationFromDates() / 60)) {
            // duration set by user does not fit within start date and end date
            toastTheUser(validAmountOfTimeCSeq);
        } else if (titleText == null || (titleText != null && task.getTitle().length() == 0)) {
            // notify user to create a title
            toastTheUser(validTitleCSeq);
        } else {
            DatabaseConnection mDbHelper = new DatabaseConnection(this);
            mDbHelper.addTask(task);

            Intent intent = new Intent(this, TaskCreatedActivity.class);
            intent.putExtra("eID", eID);
            intent.putExtra("uID", uID);
            intent.putExtra("title", titleText);
            intent.putExtra("desc", descText);
            intent.putExtra("priority", priority); // !will likely be unnecessary!
            intent.putExtra("expHours", expHours); // !Same!

            startActivity(intent);
        }
    }

    public  void onBack(View view) {
        onBackPressed();
    }

    // Back Pressing courtesy of Jax and Richard M. from:
    // http://stackoverflow.com/questions/2257963/how-to-show-a-dialog-to-confirm-that-the-user-wishes-to-exit-an-android-activity
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    /**
     * Return the date as a string formatted with pattern based on the pickDate and pickTime
     * @param pickDate the date chosen in a DatePicker
     * @param pickTime the time chosen in a TimePicker
     * @param pattern the SimpleDateFormat to base the return on
     * @return the string of the date picked
     */
    @TargetApi(Build.VERSION_CODES.M)
    private String makeDate(DatePicker pickDate, TimePicker pickTime, SimpleDateFormat pattern) {
        String realDate = null;
        if (pickDate != null) {
            int startDay = pickDate.getDayOfMonth();
            int startMonth = pickDate.getMonth();
            int startYear = pickDate.getYear() - 1900; // gregorian calendar

            int startHour, startMinute;
            if (pickTime != null) {
                startHour = pickTime.getHour();
                startMinute = pickTime.getMinute();
            } else { // choose midnight
                startHour = 0;
                startMinute = 0;
            }

            realDate = pattern.format(new Date(startYear,
                    startMonth,
                    startDay,
                    startHour,
                    startMinute));
        }
        return realDate;
    }

    /**
     * If thisDate is null, return the current Date as a String
     * according to the specified pattern.
     * @param thisDate the string date to be checked
     * @param pattern the simpledateformat to format the return
     * @return the string Date of the current date, or null
     */
    private String checkDate(String thisDate, SimpleDateFormat pattern) {
        Calendar currDate;
        String realDate = thisDate;
        if (thisDate == null) {
            // use current date
            currDate = Calendar.getInstance();
            realDate = pattern.format(new Date(currDate.YEAR - 1900,
                    currDate.MONTH,
                    currDate.DAY_OF_MONTH,
                    currDate.HOUR,
                    currDate.MINUTE));
        }
        return realDate;
    }

    /**
     * Toast the user this CharSequence msg
     * @param msg
     */
    private void toastTheUser(CharSequence msg) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }

}

