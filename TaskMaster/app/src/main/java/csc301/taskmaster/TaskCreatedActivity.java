package csc301.taskmaster;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import csc301.backend.DatabaseConnection;

import java.util.Calendar;
import java.util.List;

import csc301.backend.SimpleScheduler;
import csc301.backend.SchedulingAlgorithm;
import csc301.backend.Task;
import csc301.backend.mTask;

import static android.graphics.Color.WHITE;

public class TaskCreatedActivity extends AppCompatActivity {

    ArrayList<mTask> scheduledTasks = new ArrayList<mTask>();
    private String uID;
    private String eID;
    private int expHours;
    private int priority;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_created);

//        ArrayList<mTask> todayTasks = (ArrayList<mTask>) getIntent().getExtras().get("tasks");
//        if (todayTasks != null && !todayTasks.isEmpty()) {
//            displayTasks(todayTasks);
//        }

        uID = (String) getIntent().getExtras().get("uID");
        eID = (String) getIntent().getExtras().get("eID");
        priority = (int) getIntent().getExtras().get("priority");
        expHours = (int) getIntent().getExtras().get("expHours");

        DatabaseConnection mDbHelper = new DatabaseConnection(this);
        List<Task> allTasks = mDbHelper.getTasks();
        SchedulingAlgorithm scheduler = new SimpleScheduler();
        scheduledTasks = (ArrayList<mTask>) scheduler.scheduleToDos(allTasks);

        if (scheduledTasks != null && !scheduledTasks.isEmpty()) {
//            System.out.println("scheduled tasks = " + scheduledTasks.size());
            displayTasks(scheduledTasks);
        }

    }

    public void goToOtherOption(View activity_task_created) {
        Intent intent = new Intent(this, CalendarActivity.class);
        // Get info from day Scroll then add the task to the data base
        // Then move to CalendarActivity
        // Where the data is taken from the database and sorted
        // addtask.addTask();

        DatabaseConnection mDbHelper = new DatabaseConnection(this);
        List<Task> allTasks = mDbHelper.getTasks();
        List<Task> taskList = new ArrayList<Task>();
        for (Task t : allTasks) {
//            System.out.println(">> t: " + t.toString());
            if (t.geteID().equals(eID)) {
                t.setDuration(expHours);
                t.setPriority(priority);
                taskList.add(t);
            }
        }

        SimpleScheduler scheduler = new SimpleScheduler();
        List<mTask> scheduledTasks = scheduler.scheduleToDos(taskList);
        intent.putExtra("uID" ,uID);

        if (scheduledTasks != null && !scheduledTasks.isEmpty()) {
            displayTasks(scheduledTasks);
        }

        startActivity(intent);

    }

    public void displayTasks(List<mTask> tasks) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.task_created_linear_layout);
        // retrieving all 24 TextViews from the LinearLayout that holds them
        List<TextView> hourlyTextViews = new ArrayList<TextView>();
        if (linearLayout != null) {
            for (int i = 0; i < linearLayout.getChildCount(); i++) {
                if (linearLayout.getChildAt(i) instanceof TextView) {
                    hourlyTextViews.add((TextView) linearLayout.getChildAt(i));
                }
            }
        }
        //run through each hour and check if a task belongs on it, setting colour appropriately
        for (int hour = 0; hour < hourlyTextViews.size() ; hour ++) {
            //flag to check if any task took this slot
            boolean isHourTaken = false;
            for (mTask task : tasks) {
                isHourTaken = displayTask(hour, task, hourlyTextViews);
                if (isHourTaken) {
                    break;
                }
            }
            //if none of the tasks in the list took up this timeslot, set it to white
            if (!isHourTaken) {
                (hourlyTextViews.get(hour)).setBackgroundColor(WHITE);
            }
        }
    }

    // helper function for displayTasks
    public boolean displayTask(int hour, mTask task, List<TextView> hourlyTextViews) {
        int start = task.getStart().get(Calendar.HOUR_OF_DAY);
        int end = task.getEnd().get(Calendar.HOUR_OF_DAY);
        // if the task is scheduled on this hour, colour it in
        if (start <= hour && end >= hour) {
            TextView currHour = hourlyTextViews.get(hour);
            String time = String.valueOf(currHour.getText());
            currHour.setBackgroundColor(task.getColorFromPriority());
            currHour.setText(time + task.getTitle());
            currHour.setTextColor(WHITE);

            return true;
        }
        return false;
    }


    /**
     * Return and keep previous changes for user convenience
     * @param view the current view
     */
    public void attemptReturn(View view) {
        DatabaseConnection mDbHelper = new DatabaseConnection(this);
        mDbHelper.deleteTask(eID);
        onBackPressed();
    }

    // Back Pressing courtesy of Jax and Richard M. from:
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_input_delete)
                .setTitle("Task Reject")
                .setMessage("Are you sure you want to reject these changes?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }


}
