package csc301.taskmaster;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TaskAcceptedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_accepted);
    }
}
