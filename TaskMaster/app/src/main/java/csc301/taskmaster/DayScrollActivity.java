package csc301.taskmaster;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import csc301.backend.DatabaseConnection;
import csc301.backend.SimpleScheduler;
import csc301.backend.Task;
import csc301.backend.mTask;

import static android.graphics.Color.DKGRAY;
import static android.graphics.Color.GRAY;
import static android.graphics.Color.WHITE;

public class DayScrollActivity extends AppCompatActivity {

    private String uID;
    private Integer date;
    private ArrayList<mTask> todayTasks;

    //private Map<Integer, TextView> hourToTextView = new HashMap<Integer, TextView>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_scroll);
        uID = (String) getIntent().getExtras().get("uID");
        date = (int) getIntent().getExtras().get("date");
        System.out.println("Date given to dayscroll is " + date);

        DatabaseConnection mDbHelper = new DatabaseConnection(this);
        List<Task> tasks = mDbHelper.getTasks();
        List<Task> userTasks = new ArrayList<Task>();
        for (Task task : tasks) {
            if ((task.getHostID().equals(uID) || task.getpID().equals(uID))) {
                userTasks.add(task);
            }
        }
        List<mTask> scheduledTasks = new SimpleScheduler().scheduleToDos(userTasks);
        if (scheduledTasks != null && !scheduledTasks.isEmpty()) {
            displayTasks(scheduledTasks);
        }

        /*
        this.todayTasks = (ArrayList<mTask>) getIntent().getExtras().get("tasks");
        this.date = (Integer) getIntent().getExtras().get("date");
        if (todayTasks != null && !todayTasks.isEmpty()) {
            displayTasks(todayTasks);
        }
        */

        Button goBack = (Button) findViewById(R.id.goback);
        if (goBack != null) {
            goBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptReturn(view);
                }
            });
        }

        Button addEvent = (Button) findViewById(R.id.addevent);
        if (addEvent != null) {
            addEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    makeEvent(view);
                }
            });
        }

    }

    public void displayTasks(List<mTask> tasks) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.day_scroll_linear_layout);
        // retrieving all 24 TextViews from the LinearLayout that holds them
        List<TextView> hourlyTextViews = new ArrayList<TextView>();
        if (linearLayout != null) {
            for (int i = 0; i < linearLayout.getChildCount(); i++) {
                if (linearLayout.getChildAt(i) instanceof TextView) {
                    hourlyTextViews.add((TextView) linearLayout.getChildAt(i));
                }
            }
        }
        //run through each hour and check if a task belongs on it, setting colour appropriately
        for (int hour = 0; hour < hourlyTextViews.size() ; hour ++) {
            //flag to check if any task took this slot
            boolean isHourTaken = false;
            for (mTask task : tasks) {
                isHourTaken = displayTask(hour, task, hourlyTextViews);
                if (isHourTaken) {
                    break;
                }
            }
            //if none of the tasks in the list took up this timeslot, set it to white
            if (!isHourTaken) {
                (hourlyTextViews.get(hour)).setBackgroundColor(WHITE);
            }
        }
    }

    // helper function for displayTasks
    public boolean displayTask(int hour, mTask task, List<TextView> hourlyTextViews) {
        int start = task.getStart().get(Calendar.HOUR_OF_DAY);
        int end = task.getEnd().get(Calendar.HOUR_OF_DAY);
        // if the task is scheduled on this hour, colour it in
        if (start <= hour && end >= hour) {
            TextView currHour = hourlyTextViews.get(hour);
            String time = String.valueOf(currHour.getText());
            currHour.setBackgroundColor(task.getColorFromPriority());
            currHour.setText(time + task.getTitle());
            currHour.setTextColor(WHITE);

            return true;
        }
        return false;
    }


    public void makeEvent(View activity_day_scroll) {
        Intent intent = new Intent(this, TaskActivity.class);
        intent.putExtra("uID", uID);
        intent.putExtra("date", date);
        intent.putExtra("tasks", todayTasks);
        startActivity(intent);
    }

    public void attemptReturn(View view) {
        Intent previous = getIntent();
        String uID = (String) previous.getExtras().get("uID");
        Intent intent = new Intent(this, CalendarActivity.class);
        intent.putExtra("uID", uID);
        startActivity(intent);
    }
}


