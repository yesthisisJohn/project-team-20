package csc301.taskmaster;

import static org.junit.Assert.assertEquals;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.junit.Test;

import csc301.backend.SimpleScheduler;
import csc301.backend.Task;
import csc301.backend.mTask;


public class SimpleSchedulerTest{
	
	
	// Set up
	public Task setUpTask(String sDate, String eDate, String title, String desc, boolean isMut){
		
		Task returnTask;
		
		// Setting dummy values
		String eID = UUID.randomUUID().toString();
		String hostID = UUID.randomUUID().toString();
		String pID = UUID.randomUUID().toString();
		
		returnTask = new Task(eID, hostID, pID, sDate, eDate, title, desc, isMut);
		return returnTask;
	}
	
	
	// task is null, and calling scheduleToDos
	@Test(expected=NullPointerException.class)
	public void checkNullTask(){
		
		SimpleScheduler s = new SimpleScheduler();
		Task t = null;
		List<Task> Tasks = new ArrayList<Task>();
		
		// adding null task
		Tasks.add(t);
		
		assertEquals(null, s.scheduleToDos(Tasks));
	}
	
	// sortedTasks is null
	@Test(expected = NullPointerException.class)
	public void checkNullSortedTasks(){
		
		SimpleScheduler s = new SimpleScheduler();
		List<Task> nullTasks = null;
		assertEquals(null, s.scheduleToDos(nullTasks));

	}
	
	// Scheduling one task 
	
	@Test
	public void scheduleOneTask(){
		
		SimpleScheduler s = new SimpleScheduler();
		List<Task> oneTask = new ArrayList<Task>();
		Task current;
		current = setUpTask("2016-12-06 12:00:00", "2016-12-07 20:00:00", "Assignment1", "Finish It.", true);
		oneTask.add(current);
		List<mTask> ftasks = s.scheduleToDos(oneTask);
		
		Iterator<mTask> taskIterator = ftasks.iterator();
		System.out.println("\n");
		while(taskIterator.hasNext()){
			System.out.println(taskIterator.next().toString());
		}
		
		System.out.print(ftasks.size());
		
	}

	// Two tasks scheduled, task 1 is mutable
	@Test
	
	public void scheduleTwoTasks(){
		SimpleScheduler s = new SimpleScheduler();
		List<Task> twoTasks = new ArrayList<Task>();
		Task task1, task2;
		task1 = setUpTask("2016-12-06 12:00:00", "2016-12-07 20:00:00", "Assignment1", "Finish It.", true);
		task2 = setUpTask("2016-12-06 13:00:00", "2016-12-06 20:00:00", "Assignment2", "Finish It.", true);
		
		//adding the two tasks
		twoTasks.add(task1);
		twoTasks.add(task2);
		
		List<mTask> ftasks = s.scheduleToDos(twoTasks);
		
		Iterator<mTask> taskIterator = ftasks.iterator();
		System.out.println("\n");
		while(taskIterator.hasNext()){
			System.out.println(taskIterator.next().toString());
		}
		
		System.out.print(ftasks.size());
		System.out.println("\n");
		System.out.println("\n");
	}
	
	
}
