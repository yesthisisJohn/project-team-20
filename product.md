# TaskMaster

#### Q1: What are you planning to build?
---
An app that helps students and young professionals plan their schedules. Being students ourselves, we often feel that we're running out of time, constantly borrowing it from other activities like sleeping, exercising, etc. To address this problem, we will develop a tool which will generate an optimal schedule that maximizes productivity without giving up other responsibilities. This app -- which we have dubbed TaskMaster -- will allow the user to input a schedule they must conform to (e.g., their school or work schedules), and the app will then output a new schedule indicating when they should be working on their given tasks. TaskMaster will also allow users to share their schedule with others, facilitating easy meeting scheduling.  Users can request meetings with other users, who can then accept, decline, or modify the meeting.

#### Q2: Who are your target users?
---
Our application is geared towards students and young professionals, especially those who have enough work to feel overwhelmed but are inefficient at time management. The list is not exhaustive though.  Anyone who lacks efficiency in their time management is encouraged to try this application.

**[Personas](https://app.xtensio.com/folio/7yjck67x)**

**[Taskmaster Demo](https://youtu.be/bf3NzYP6GGE)**

#### Q3: Why would your users choose your product? What are they using today to solve their problem?
---
TaskMaster allows its users to easily upload their schedule to be optimized. 
It provides a fast and easy way for the user to decide how their time should be best spent. Currently, there are apps ranging from SolCalendar to Shifts available to set up habits, to stay focused, and do other task management activities; however none address the fundamental problem of how one should schedule their time to fully optimize productivity. 

Some calendar applications offer special widgets offering location-based weather information for their users--*SolCalendar* is one such application. Such a feature would be deemed unnecessary by a student who must get tasks A, B, and C, done rain or shine. Calendars like *Shifts* for iOS offer amazing scheduling and recording shifts so users can track their time usage, set rules for recurring events, overlay their shifts on other calendars, and even enter hourly rates for estimated earnings for each shift. The average wild-eyed young adult seeks to become such a time-management hero--an individual capable of knowing what to do, when to do it, how long to do it, and for how much money. Taskmaster acts as a true stepping stone towards such heights of time management.

TaskMaster provides a comprehensive solution to all the problems associated with time management. Of course there is the alternative of scheduling your tasks yourself, but studies show that **[having to make too many small choices depletes our mental energy] (http://psycnet.apa.org/journals/psp/74/5/1252/)** and **[lowers our self-control] (http://psycnet.apa.org/journals/psp/94/5/883/)** , which is detrimental to good time management.
Using TaskMaster instead is guaranteed to provide you with the optimal way to get the most done, and put your time to better use.
