# CSC301 Group Project - Team 20
 [Slack](https://csc301-project.slack.com) | [Asana](https://app.asana.com) | al@alberttai.com |
 Meeting: Tuesdays 5-6pm 
 ---

## [TaskMaster](https://github.com/csc301-fall-2016/project-team-20/tree/master/TaskMaster)
In this directory, you will find an Android Studio project containing all resources pertaining to our application.

## [product.md](https://github.com/csc301-fall-2016/project-team-20/blob/master/product.md)
In this file, you will find a description of what our product hopes to accomplish and to whom our product targets. We also discuss why our product will be superior to alternatives that currently exist. 


## [Iteration 1](https://github.com/csc301-fall-2016/project-team-20/tree/master/Iteration-1)
In this directory, you will find files associated to Devlierable 1, these files include:
- iteration-01.plan.md
- iteration-01.review.md
- MeetingLogs.md

## [Research](https://github.com/csc301-fall-2016/project-team-20/tree/master/Research)
In this directory, we have included all of our research findings regarding developing an Android app that interfaces with various existing Calendar APIs as well as possible data structure / algorithms that may help us in developing a scheduling algorithm. 
