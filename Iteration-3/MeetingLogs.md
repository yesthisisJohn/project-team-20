Tuesday, November 22, 2016 - 5:10 pm to 8:00 pm
---
- With the complications of login and due to the fact it is not a key aspect in what we want our application to achieve, we have 
  decided to remove it for now and focus on other key implementations.
- Received updates on everyones tasks. Pooled in knowledge to help group members struggling with certain concepts and design.

Tuesday, November 29, 2016 - 5:10 pm to 8:00 pm
---
- Planned group sessions for this weekend (Dec 1 - Dec 3) to finalize product.
- Divided members into groups of 2 - 3 to focus on key tasks such as the algorithm for scheduling etc.
- Assigned tasks on Asana to ensure each member remembers what they are going.
- Created a diagram to clearly identify the changes to the implementation.
