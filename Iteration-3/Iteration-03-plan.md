## Iteration 03 
- Start Date: November 17, 2016
- End Date: December 5, 2016

#### Roles & Responsibilities
Roles are assigned based on prior/ongoing course experience. 
 - Victor Li: Has experience working in front-end of Android Studio from CSC207 and has proven to be
   extremely capable in implementing backend features with great design. 
 - Ana has done front-end work for CSC207 and has the ability to read and understand code as well as learn
   very quickly.
 - Sammy has also done front-end work for CSC207.
 - John has primarily worked on back-end and has shown that he has a strong grasp in this field.
   He has the ability to constantly monitor a task and to make sure everything is going in the right direction.
 - MingChen has demonstrated a high degree of intelligence in problem solving and has shown that he has an interest
   in designing algorithms. 
 - Albert and Victor Wu are taking CSC343 (databases) and have a high aptitude in writing SQL code.


Albert: 
- Maintaining the SQL server, writing queries, schemas etc
- Streamlining the user experience

Ana:
- Linking front and backend, making necessary changes to ensure that data is passed properly between activities

MingChen:
- Backend algorithm for scheduling

John:
- Project manager: Delegating tasks and ensure that everyone is contributing
- Work on the homepage of the application

Victor Li:
- Writing front end XML code to display calendar and other GUI components that heighten user experience
- Help with implementation in backend for task classes and in addition to the algorithm if required.

Victor Wu:
- Testing database and ensuring everything is being stored correctly
- Maintaining SQL server

Sammy:
- Testing the application (from a users perspective)
- Help implementation of the front end for tasks

#### Goals for end of iteration
- Displaying different views (weekly, monthly)
- Have algorithm correctly scheduling events
- Improve UI from iteration 2
- Complete the homepage
- Add any additional features that will improve user experience
- Develop stronger teamwork skills for future endeavors :)
