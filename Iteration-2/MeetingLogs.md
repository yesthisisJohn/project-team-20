November 8
---
- Got an update from each member and discussed complications/progress made
- Set new deadlines and reassigned tasks to balance workload and further the development of key features
- Planned possible all day skype session on Nov 12/13
- Realised a major complication for each member was how to integrate the backend code with the frontend parts that have not been 
  made yet. Further research is REQUIRED by each member by this Friday (Nov 11)

November 15
---
- Merged individual portions of everyones tasks
- Assigned new tasks to be accomplished by the next meeting
- Set up a new meeting later on tonight to finalize iteration 2
- Divided into groups to focus on key features that need to be finished for future progression
