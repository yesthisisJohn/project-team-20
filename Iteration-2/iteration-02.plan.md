# TaskMaster
## Iteration 02

- Start Date: October 27, 2016
- End Date: November 16, 2016


#### Roles & Responsibilities

Albert:

- Secretarial duties
- Delegation of tasks
- Setting up server


Ana:

- Google login screen
- Login authentication
- Testing


Mingchen:

- Backend classes
- Testing


John:

- Delegation of tasks
- Marketing research
- Backend


Sammy:

- Front-end loading screen
- Backend transition to login screen


Victor Li:

- Front-end home page
- Front-end calendar
- Back-end calendar


Victor Wu:

- PostgreSQL connection and database reading/writing
- Assist with backend classes


#### Goals for end of iteration

- Working login screen authorization
- Displaying calendar
- Basic functionality of viewing calendars from google account
- Reading in events and info into local calendar
- Actually use Asana


##### **NOTICE:**
Both plan and review markdown files were created today. However, we set the plans through Asana and
our Facebook groupchat weeks in advance (approximately around November 1st).
