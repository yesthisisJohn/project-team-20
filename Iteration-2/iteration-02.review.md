# TaskMaster

# Iteration 02 - Review 

- When: Nov 1, 9, 15, 2016
- Where: BA2270


# Process - Reflection


#### Product - Review 
Completed tasks:

- Some basic PostgreSQL commands to create tables, select, insert commands
- Loading screen, login screen
- Made use of Asana for task assignment


#### Not completed

- Not able to connect to server database yet
- Backend algorithms not yet implemented
- Backend not reading in calendar events yet properly from Google

